/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.robotnewproject;

/**
 *
 * @author Kitty
 */
public class bomb {

    private int x;
    private int y;
    private char symbol = 'b';

    public bomb(int x, int y) {
        this.x = x;
        this.y = y;
        this.symbol = 'b';
    }

    public int getx() {
        return x;
    }

    public int gety() {
        return y;
    }

    public char getsymbol() {
        return symbol;
    }
    public boolean ison(int x, int y) {
        return this.x == x && this.y == y;
    }
}
