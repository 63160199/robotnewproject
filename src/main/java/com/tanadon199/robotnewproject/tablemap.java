/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.robotnewproject;

/**
 *
 * @author Kitty
 */
public class tablemap {

    private int width;
    private int height;
    private robot Robot;
    private bomb Bomb;

    public tablemap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobot(robot Robot) {
        this.Robot = Robot;
    }

    public void setBomb(bomb Bomb) {
        this.Bomb = Bomb;
    }

    public void showmap() {
        System.out.println("Map");
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (Robot.getX() == x && Robot.getY() == y) {
                    showrobot();
                } else if (Bomb.ison(x, y)) {
                    System.out.print(Bomb.getsymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }

    private void showrobot() {
        System.out.print(Robot.getSymbol());
    }

    public boolean inmap(int x, int y) {
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isbomb(int x, int y) {
        return Bomb.ison(x, y);
    }

}
