/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.robotnewproject;

import java.util.Scanner;

/**
 *
 * @author Kitty
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        tablemap map = new tablemap(10,10);
        robot Robot = new robot(2,2,'x',map);
        bomb Bomb = new bomb(5,5);
        map.setRobot(Robot);
        map.setBomb(Bomb);
        while(true){
            map.showmap();
            char direction = inputDirection(sc);
            if(direction=='q'){
                System.out.println("Bye Bye!!!");
            }
            Robot.walk(direction);
        }
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        char direction = str.charAt(0);
        return direction;
    }
}

     