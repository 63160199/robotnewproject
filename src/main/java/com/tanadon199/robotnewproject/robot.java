/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.robotnewproject;

/**
 *
 * @author Kitty
 */
public class robot {

    private int x;
    private int y;
    private char symbol;
    private tablemap map;

    public robot(int x, int y, char symbol, tablemap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'N':
            case 'w':
                if (map.inmap(x, y - 1)) {
                    y = y - 1;
                }
                break;
            case 'S':
            case 's':    
                if (map.inmap(x, y + 1)) {
                    y = y + 1;
                }
                break;
            case 'E':
            case 'd':     
                if (map.inmap(x + 1, y)) {
                    x = x + 1;
                    break;
                }
            case 'W':
            case 'a':     
                if (map.inmap(x - 1, y)) {
                    x = x - 1;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    public boolean ison(int x, int y) {
        return this.x == x && this.y == y;
    }
}
